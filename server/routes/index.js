(function() {
 
  'use strict';
  var express = require('express');
  var router = express.Router();
  var mongojs = require('mongojs');
  var db = mongojs('meangc', ['gcs', 'blogs']);
 
  /* GET home page. */
  router.get('/', function(req, res) {
    res.render('index');
  });
 
  /* BLOG BLOCK */
  router.get('/api/blogs', function(req, res) {
    db.blogs.find(function(err, data) {
      res.json(data);
    });
  });
  router.get('/api/blogs/:blogID', function(req, res) {

    db.blogs.find({
      'blogID': req.params.blogID
    }, function(err, data) {
      res.json(data);
    });
    
  });
  router.put('/api/blogs', function(req, res) {
    db.blogs.update({
      _id: mongojs.ObjectId(req.body._id)
    }, {
      //Add DB columns here
      blogID: req.body.blogID,
      blogTitle: req.body.blogTitle,
      blogDesc: req.body.blogDesc,
      blogContent: req.body.blogContent,
      blogPic: req.body.blogPic,
      blogDate: req.body.blogDate
    }, {}, function(err, data) {
      res.json(data);
    });
  });
  router.post('/api/blogs', function(req, res) {
    db.blogs.insert(req.body, function(err, data) {
      res.json(data);
    });
  });
  router.delete('/api/blogs/:_id', function(req, res) {
    db.blogs.remove({
      _id: mongojs.ObjectId(req.params._id)
    }, '', function(err, data) {
      res.json(data);
    });
  });


  /* GC CONTENT BLOCK */
  router.get('/api/gcs', function(req, res) {
    db.gcs.find(function(err, data) {
      res.json(data);
    });
  });
 
  router.post('/api/gcs', function(req, res) {
    db.gcs.insert(req.body, function(err, data) {
      res.json(data);
    });
 
  });
 
  router.put('/api/gcs', function(req, res) {
 
    db.gcs.update({
      _id: mongojs.ObjectId(req.body._id)
    }, {
      //Add DB columns here
      header: req.body.header,
      percent: req.body.percent
    }, {}, function(err, data) {
      res.json(data);
    });
 
  });
 
  router.delete('/api/gcs/:_id', function(req, res) {
    db.gcs.remove({
      _id: mongojs.ObjectId(req.params._id)
    }, '', function(err, data) {
      res.json(data);
    });
 
  });
 
  module.exports = router;
 
}());