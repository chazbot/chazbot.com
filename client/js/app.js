gcApp = angular.module('gcApp', ['ngRoute'])
  .config(function($routeProvider) {
    $routeProvider
      .when('/gc', {
        templateUrl: 'partials/gc.html',
        controller: 'gcCtrl'
      }).when('/expression', {
        templateUrl: 'partials/expression.html',
        controller: 'expCtrl'
      }).when('/about', {
        templateUrl: 'partials/about.html',
        controller: 'expCtrl'
      }).when('/anchor', {
        templateUrl: 'partials/anchor.html',
        controller: 'anchorCtrl'
      })/*.when('/blogs', {
        templateUrl: '/partials/blogs.html',
        controller: 'blogCtrl'
      }).when('/blogs/:param', {
        templateUrl: '/partials/blog.html',
        controller: 'blogCtrl'
      }).when('/blogadd', {
        templateUrl: '/partials/blogadd.html',
        controller: 'blogCtrl'
      })*/.when('/', {
        templateUrl: 'partials/home.html',
        controller: 'gcCtrl'
      }).otherwise({
        redirectTo: '/'
      });
  });
