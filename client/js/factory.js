gcApp.factory('blogsFactory', function($http) {
  var urlBase = '/api/blogs';
  var _blogService = {};

  _blogService.getblogs = function() {
    return $http.get(urlBase);
  };
 
  _blogService.saveblog = function(blog) {
    return $http.post(urlBase, blog);
  };

  _blogService.getblog = function(id) {
    return $http.get(urlBase + '/' + id);
  };
 
  _blogService.deleteblog = function(id) {
    return $http.delete(urlBase + '/' + id);
  };
 
  return _blogService;
});

gcApp.factory('gcsFactory', function($http) {
  var urlBase = '/api/gcs';
  var _gcService = {};
 
  _gcService.getgcs = function() {
    return $http.get(urlBase);
  };
 
  _gcService.savegc = function(gc) {
    return $http.post(urlBase, gc);
  };
 
  _gcService.updategc = function(gc) {
    return $http.put(urlBase, gc);
  };
 
  _gcService.deletegc = function(id) {
    return $http.delete(urlBase + '/' + id);
  };
 
  return _gcService;
});