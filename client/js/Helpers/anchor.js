(function ($) {

    //function for a positional translation table for gaps
    var paddedKey = function (seq, seqPadded) {

        var result = [];

        for (var i = 0; i < seq.length; i++){

            if (i == 0){
                for (var j = 0; j < seqPadded.length; j++){
                    if (seq.charAt(i) == seqPadded.charAt(j)){
                        result[i] = j;
                        break;
                    }else{
                        continue;
                    }
                }
            }else{
                for (var j = result[i-1] + 1; j < seqPadded.length; j++){
                    if (seq.charAt(i) == seqPadded.charAt(j)){
                        result[i] = j;
                        break;
                    }else{
                        continue;
                    }
                }
            }
        }
        //console.log(result)
        return result;

    }

    // DOM Elements
    var $fileInput = $('#file'),
        $submitBtn = $('#submit'),
        $continueBtn = $('#continue'),
        $clearBtn = $('#clear'),
        $exportBtn = $('#export'),
        $dpf = $('#dpf'),
        $dpfs = $('#dpfs'),
        $dff = $('#dff'),
        $dffs = $('#dffs'),
        $ta = $('#ta'),
        $tapf = $('#tapf');

    //Global Vars
    var $fastaInp = new Array(),
        $aaCode = "ACDEFGHIKLMNPQRSTVWY",
    	$anchor,
		$anchorSeqUnpadded,
		$anchorSeqUnpaddedLength,
        $anchorTrans,
        $myCSV = '';

    //Export CSV file
    var onExport = function(e) {
        window.open('data:text/csv;charset=utf-8,' + escape($myCSV));
    }

    //Filter on pivot residues
    var onContinue = function (e) {

    	var pivot = $dpfs.val();
    	var calc = new Array();
    	for (var i = 0; i < $fastaInp.length;i++){
    		calc[i] = $fastaInp[i].seq.toUpperCase().split('');
    	}

        $anchorTrans = paddedKey($anchorSeqUnpadded, $anchor.seq)

        //initialize numerical array for results
        var results = new Array();
        for (var i = 0; i < $aaCode.length; i++){
            results[$aaCode.charAt(i)] = Array.apply(null, new Array($anchor.seq.length)).map(Number.prototype.valueOf,0);
        }
        results["other"] = Array.apply(null, new Array($anchor.seq.length)).map(Number.prototype.valueOf,0);

        console.log(pivot)
        var passPivot = 0;
        for (var i = 0; i < calc.length; i++){
            var match = true;
            var cur = calc[i];
            for (var j = 0; j < pivot.length; j++){
                if (cur[$anchorTrans[pivot[j]]] != $anchor.seq.charAt($anchorTrans[pivot[j]])){
                    console.log("mismatch")
                    match = false;
                    break;
                }
            }
            if (match){
                passPivot++;
                for (var j = 0; j < cur.length; j++){
                    if ($aaCode.indexOf(cur[j]) == -1){
                        results["other"][j]++;
                    }else{
                        results[cur[j]][j]++;
                    }
                }
            }
        }

        for (var key in results){
            for (var j = 0; j < results[key].length; j++){
                console.log("happening")
                results[key][j] = parseFloat(results[key][j] / passPivot);
            }
        }

        $myCSV = 'Padded Coordinates,' + Array.apply(null, Array($anchor.seq.length + 1)).map(function (_, i) {return i + 1;}).join(',');
        $myCSV += '\nAnchor Sequence,' + $anchor.seq.split('').join(',')

        pivotCSV = [];
        for (var i = 0; i < pivot.length; i++){
            pivotCSV[$anchorTrans[pivot[i]]] = '#'
        }

        $myCSV += '\nPivot Residues,' + pivotCSV.join(',')        

        for(var i = 0; i < $aaCode.length; i++){
            $myCSV += '\n' + $aaCode.charAt(i) + ',' + results[$aaCode.charAt(i)].join(',');
        }
        $myCSV += '\nother,' + results['other'].join(',');

        $dffs.multiSelect({keepOrder:true});
        $dff.css("display","inline");
        $tapf.text(passPivot);

    }
    //Legacy Button (reintegrate) 
    var onClear = function (e) {
    	
    }

    // On upload of FASTA alignment
    var onUpload = function (e) {
        e.preventDefault();
        $submitBtn.prop("disabled",true);
 
        // Validate is file
        var fileList = $fileInput.get(0).files;
        if (fileList.length === 0) {
            fail('You must select a file');
        }
 
        // Validate is CSV
        var file = fileList[0];
         
        // Init file reader and read file
        var fileReader = new FileReader();
        fileReader.onload = function (e) {
            
            //Parse FASTA alignment files
        	var fasta = require('fasta-parser')
        	var parser = fasta()
			parser.on('data', function(data) { $fastaInp.push(JSON.parse(data.toString())) })
			parser.write(fileReader.result)
			parser.end()

			//Pad lengths of FASTA seqs
			var aliLength;
			for (var i = 0; i < $fastaInp.length;i++){
				if (i == 0){
					aliLength = $fastaInp[i].seq.length;
				}else{
					var diff = aliLength - $fastaInp[i].seq.length;
					if (diff > 0){
						var pad = "";
						while (pad.length < diff){
							pad +='-';
						}
						$fastaInp[i].seq += pad; 
					}
				}
			}

			$anchor = $fastaInp[0];
			$anchorSeqUnpadded = $anchor.seq.replace(/-/g,'')
			$anchorSeqUnpaddedLength = $anchorSeqUnpadded.length

			for(var i = 0; i < $anchorSeqUnpaddedLength;i++){
				$dpfs.append($("<option></option>")
					.attr("value", i)
					.text((i + 1) + " " + $anchorSeqUnpadded.charAt(i)));
                $dffs.append($("<option selected=\"selected\"></option>")
                    .attr("value", i)
                    .text((i + 1) + " " + $anchorSeqUnpadded.charAt(i)));
			}
            $dpfs.multiSelect({keepOrder:true});
			$dpf.css("display","inline");
            $ta.text($fastaInp.length);

        };

        fileReader.onerror = function (e) {
            throw 'Error reading FASTA file';
        };
 
        // Start reading file
        fileReader.readAsText(file);
    };
 
    // Bind upload button
    $submitBtn.on('click', onUpload);
    $clearBtn.on('click', onClear);
    $continueBtn.on('click', onContinue);
    $exportBtn.on('click', onExport);
})(jQuery);