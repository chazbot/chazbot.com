gcApp.controller('expCtrl', function($rootScope, $scope, $sce, $routeParams) {

});

gcApp.controller('anchorCtrl', function($rootScope, $scope, $sce, $routeParams) {

});

gcApp.controller('blogCtrl', function($rootScope, $scope, $sce, $routeParams, blogsFactory) {
  
  $scope.blogs = [];
  $scope.param = $routeParams.param;

  $scope.trust = $sce.trustAsHtml;

  if ($scope.param) {
    console.log("pre-fetch");
    blogsFactory.getblog($scope.param).then(function(data) {
      $scope.single_blog = data.data;
      console.log($scope.single_blog);
      //$scope.html = $scope.single_blog.blogContent;
      
    });
    
  }

  blogsFactory.getblogs().then(function(data) {
    $scope.blogs = data.data;
  });

  $scope.save = function(entry) {
    
    blogTitle = $scope.entry.blogTitle;
    blogID = $scope.entry.blogID;
    blogDesc = $scope.entry.blogDesc;
    blogContent = $scope.entry.blogContent;
    blogPic = $scope.entry.blogPic;
    blogDate = new Date();

    blogsFactory.saveblog({
      "blogTitle": blogTitle,
      'blogID': blogID,
      'blogDesc': blogDesc,
      "blogContent": blogContent,
      "blogPic": blogPic,
      'blogDate': blogDate
    }).then(function(data) {
      $scope.blogs.push(data.data);
    });

    $scope.entry.blogTitle = '';
    $scope.entry.blogID = '';
    $scope.entry.blogDesc = '';
    $scope.entry.blogContent = '';
  };

  $scope.delete = function(blog) 
{    blogsFactory.deleteblog(blog._id).then(function(data) {
      if (data.data) {
        $scope.blogs.splice($scope.blogs.indexOf(blog), 1);
      }
    });
  };
});


/*GC Content Calculator Controller*/
gcApp.controller('gcCtrl', function($rootScope, $scope, gcsFactory) {

  $scope.gcs = [];
 
  // Save an entry to the server
  $scope.save = function($event) {
    
    //Stop extra line breaks
    if ($event.which == 13) {
      $event.preventDefault();
    }

    //Handle input
    if ($event.which == 13 && $scope.gcInput) {

    	//Define variables
      var header, seq, gcp, tm, count, wA ,xT, yG, zC;

      //console.log($scope.gcInput.indexOf('\n'));

      //Try to parse FASTA header
      if ($scope.gcInput.charAt(0) == '>'){
        console.log("FASTA Detected");
        header = $scope.gcInput.substr(0, $scope.gcInput.indexOf('\n'));
        seq = $scope.gcInput.substr($scope.gcInput.indexOf('\n'), $scope.gcInput.length);//
        seq = seq.replace(/\n|\s/g,'');
      
      //if no header continue as if raw sequence
      }else{
        console.log("NOT FASTA");
        header = "Unnamed Sequence";
        seq = $scope.gcInput.replace(/\n|\s/g,'');
      }

      //Do seq counts
      seq = seq.toUpperCase();
      wA = seq.split('A').length - 1;
      xT = seq.split('T').length - 1;
      yG = seq.split('G').length - 1;
      zC = seq.split('C').length - 1;

      if (wA + xT + yG + zC != seq.length){
      	alert("Please enter a DNA sequence with no degenerate nucleotides in FASTA or plain format!");
      	return;
      }

      //Do GC Percent Calculations
      count = (yG + zC);
      gcp = (count/seq.length * 100).toFixed(1) + "%";

      //If under 50bp, do TM calculations
      /*if (seq.length < 50) {
      	if (seq.length < 14){
      		tm = (wA + xT) * 2 + (yG + zC) * 4 - 16.6 * Math.log10(0.050) + 16.6 * Math.log10(0.050);
      	}else{
      		tm = 100.5 + (41 * (yG + zC) / (wA + xT + yG + zC)) - (820 / (wA + xT + yG + zC)) + (16.6 * Math.log10(0.050));
      	}
      	console.log(tm);
      }*/

      console.log(header);
      console.log(seq.length);
      console.log($scope.gcs);
      $scope.gcs.push({
        "header": header,
        "length": seq.length,
        "percent": gcp
      });

      $scope.gcInput = '';
    }
  };
});